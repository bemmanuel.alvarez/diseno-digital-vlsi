LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY main IS
	PORT(RST1,RST2,CLK_M,SEL: IN STD_LOGIC;
		  SAL: OUT INTEGER RANGE 0 TO 7);
END ENTITY;

ARCHITECTURE BEAS OF main IS

COMPONENT REG1
	PORT(CLK,RST: IN STD_LOGIC;
	      Q: OUT INTEGER RANGE 0 TO 15);
END COMPONENT;

COMPONENT REG2
	PORT(CLK,RST: IN STD_LOGIC;
	      Q: OUT INTEGER RANGE 0 TO 15);
END COMPONENT;

COMPONENT DIVf 
	GENERIC(F: INTEGER := 24);
	PORT(CLK_MST: IN STD_LOGIC;
	     CLK: OUT STD_LOGIC);
END COMPONENT;

COMPONENT MUX
	PORT(A,B: IN INTEGER RANGE 0 TO 15;
		  SEL: IN STD_LOGIC;
	     C: OUT INTEGER RANGE 0 TO 15);
END COMPONENT;

COMPONENT DECO 
	PORT(NUM: IN INTEGER RANGE 0 TO 15;
	     SAL: OUT INTEGER RANGE 0 TO 7);
END COMPONENT;

SIGNAL DF: STD_LOGIC;
SIGNAL DAT1,DAT2,NUMERO: INTEGER RANGE 0 TO 15;

BEGIN
	
	B1: REG1 PORT MAP(DF,RST1,DAT1);
	B2: REG2 PORT MAP(DF,RST2,DAT2);
	B3: DIVf GENERIC MAP(F => 24 999) PORT MAP(CLK_M, DF);
	B4: MUX PORT MAP(DAT1,DAT2,SEL,NUMERO);
	B5: DECO PORT MAP(NUMERO, SAL);
	
END BEAS;
